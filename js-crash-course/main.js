// console.log("Hello world");
// console.error("This is an error");
// console.warn("This is a warning");

// // string, numbers, boolean, null, undefined
// const age = 30;
// const name = "John";
// const rating = 4.5;
// const isCool = true;
// const x = null;
// const y = undefined;
// let z;

// // concatenation
// console.log("concatenation: ", "My name is " + name + " and I am " + age);

// // template string
// const hello = `My name is ${name} and I am ${age}`;
// console.log("template string: ", hello);

// const s = "Hello, World";
// // count character
// console.log(s.length);

// // convert to uppercase
// console.log("convert to uppercase: ", s.toUpperCase());

// // pick character (index start, total)
// console.log("select character index 0 - 5: ", s.substring(0, 5));

// // split each character in array
// console.log("split each character: ", s.split(""));

// // split each word in array
// console.log("split word: ", s.split(", "));

// // Array - variables that hold multiple values
// const fruits = ["apples", "oranges", "pears"];

// // manipulating values
// fruits[2] = "jeruk";

// // add value at the end of index
// fruits.push("mangos");

// // add value at the beginning of index
// fruits.unshift("strawberries");

// // remove the last value
// fruits.pop();

// // check if it's an array
// console.log("is it Array?: ", Array.isArray(fruits));

// // check which index of the value
// console.log("which index: ", fruits.indexOf("oranges"));

// console.log(fruits);

// const person = {
//   firstName: "Alamsyah",
//   lastName: "Bahar",
//   age: 27,
//   hobbies: ["music", "movies"],
//   address: {
//     street: "Jl.H.Mashuri",
//     city: "Purwokerto",
//   },
// };

// console.log("all data : ", person);

// // select value specific
// console.log("select index values: ", person.hobbies[1]);
// console.log("select name values: ", person.address.city);

// // destructuring
// const {
//   firstName,
//   lastName,
//   address: { city },
// } = person;

// console.log("destructuring data firstName: ", firstName);
// console.log("destructuring data city: ", city);

// // add property
// person.email = "nc.alamsyah@gmail.com";

// // object in array
// const todos = [
//   {
//     id: 1,
//     text: "Take out trash",
//     isCompleted: true,
//   },
//   {
//     id: 2,
//     text: "Meeting with boss",
//     isCompleted: true,
//   },
//   {
//     id: 3,
//     text: "Cleaning room",
//     isCompleted: false,
//   },
// ];

// console.log("all todos data: ", todos);

// // convert to JSON
// const todoJSON = JSON.stringify(todos);

// console.log("converted to json: ", todoJSON);

// // for loop
// for (let i = 0; i < 10; i++) {
//   console.log(`for loop: ${i}`);
// }

// // while loop
// let i = 0;
// while (i < 10) {
//   console.log(`while loop: ${i}`);
//   i++;
// }

// // loop the total of values
// for (let i = 0; i < todos.length; i++) {
//   console.log("print todos text: ", todos[i].text);
// }

// for (let todo of todos) {
//   console.log("loop todos id: ", todo.id);
// }

// // forEach
// todos.forEach(function (todo) {
//   console.log("foreach: ", todo.text);
// });

// // map
// const todoTextMap = todos.map(function (todo) {
//   return todo.text;
// });
// console.log("map: ", todoTextMap);

// // filter
// const todoTextFilter = todos.filter(function (todo) {
//   return todo.isCompleted === true;
// });
// console.log("filter isCompleted: ", todoTextFilter);

// // mapping the filter
// const todoTextFilterMap = todos
//   .filter(function (todo) {
//     return todo.isCompleted === true;
//   })
//   .map(function (todo) {
//     return todo.text;
//   });
// console.log("map text filter: ", todoTextFilterMap);

// // if
// const x1 = 20;

// if (x1 === 10) {
//   console.log("if 1: ", "x is 10");
// } else if (x > 10) {
//   console.log("if 1: ", "x is greater than 10");
// } else {
//   console.log("if 1: ", "x is less than 10");
// }

// const x2 = 4;
// const y2 = 11;

// if (x2 > 5 || y2 > 10) {
//   console.log("if: ", "x2 is more than 5 or y2 is more than 10");
// }

// // ternary
// const x3 = 10;
// const color = x > 10 ? "red" : "blue";
// console.log("ternary: ", color);

// // switch
// switch (color) {
//   case "red":
//     console.log("switch: ", "color is red");
//     break;
//   case "blue":
//     console.log("switch: ", "color is blue");
//     break;
//   default:
//     console.log("switch: ", "color is NOR red or blue");
//     break;
// }

// // functions
// function addNums(num1 = 2, num2 = 5) {
//   return num1 + num2;
// }
// console.log("2 + 5= ", addNums());

// // arrow function
// const addNumber = (num1, num2) => num1 + num2;
// console.log("arrow func 5 + 4= ", addNumber(5, 4));

// OOP
// coonstructor function
// function Person(firstName, lastName, dob) {
//   this.firstName = firstName;
//   this.lastName = lastName;
//   this.dob = new Date(dob);
//   this.getBirthYear = function () {
//     return this.dob.getFullYear();
//   };
//   this.getFullName = function () {
//     return `${this.firstName} ${this.lastName}`;
//   };
// }

// ***Class
// class Person {
//   constructor(firstName, lastName, dob) {
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob = new Date(dob);
//   }
//   getBirthYear() {
//     return this.dob.getFullYear();
//   }
//   getFullName() {
//     return `${this.firstName} ${this.lastName}`;
//   }
// }

// //instantiate object
// const person1 = new Person("Alamsyah", "Bahar", "12-25-1994");

// console.log("get Full Year: ", person1.getBirthYear());
// console.log("OOP firstname: ", person1.firstName);
// console.log("get Full Name: ", person1.getFullName());
// console.log("all data person1: ", person1);

// //Single Element
// console.log(document.getElementById("my-form"));
// console.log(document.querySelector(".container"));

// //Multiple Element
// console.log(document.querySelectorAll(".item"));
// console.log(document.getElementsByClassName("item"));

// const items = document.querySelectorAll(".item");

// items.forEach((item) => console.log("forEach item: ", item));

// // select items element
// const ul = document.querySelector(".items");

// // remove elements
// ul.remove();

// // remove last element
// ul.lastElementChild.remove();

// // manipulate data
// ul.firstElementChild.textContent = "Hello";
// ul.children[1].innerText = "Alam";
// ul.lastElementChild.innerHTML = "<h1>Hello</h1>";

// // select btn element
// const btn = document.querySelector(".btn");
// btn.style.background = "red";

// // event listener
// btn.addEventListener("click", (e) => {
//   e.preventDefault();
//   //   alert("button submit clicked");
//   //   console.log(e.target.className);
//   document.querySelector("#my-form").style.background = "cyan";
//   document.querySelector("body").classList.add("bg-dark");
// });

const myForm = document.querySelector("#my-form");
const nameInput = document.querySelector("#name");
const emailInput = document.querySelector("#email");
const msg = document.querySelector(".msg");
const userList = document.querySelector("#users");

myForm.addEventListener("submit", onSubmit);

function onSubmit(e) {
  e.preventDefault();
  if (nameInput.value === "" || emailInput.value === "") {
    msg.classList.add("error");
    msg.innerHTML = "Please enter all fields";

    setTimeout(() => msg.remove(), 3000);
  } else {
    const li = document.createElement("li");
    li.appendChild(
      document.createTextNode(`${nameInput.value} : ${emailInput.value}`)
    );
    userList.appendChild(li);

    // clear fields
    nameInput.value = "";
    emailInput.value = "";
  }
}
